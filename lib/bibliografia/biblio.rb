#Clase que representa la bliblioteca 

class Biblioteca
   attr_reader :autor, :isbn, :titulo, :fecha_publicacion, :serie, :editorial, :num_edicion
   def initialize(autor,isbn,titulo,fecha,editorial,edicion)
      #excepciones para que siempre tenga que haber al menos 1 de cada elemento
      raise TypeError, "Error, Debe existir al menos 1 autor." if autor.equal?""
      raise TypeError, "Error, Debe existir al menos 1 ISBNr." if isbn.equal?""
      raise TypeError, "Error, Debe existir un titulo." if titulo.equal?""
      raise TypeError, "Error, Debe existir una editorial." if editorial.equal?""
      raise TypeError, "Error, Debe existir al menos 1 edicion." if edicion.equal?""
      #variables de instancia
		@autor=autor
		@isbn=isbn
		@titulo=titulo
		@fecha_serie=fecha
		@editorial=editorial
		@num_edicion=edicion
   end
   
#metodo que devuelve el listado de autores
   def autor
	
		 @autor
	
   end
   
#metodo que devuelve el listado de isbn
   def isbn
   	 	@isbn
   	 
   end
  
  #metodo que devuelve el titulo del libro
  
   def titulo
   	 	@titulo
   	 
   end
  #metodo que devuelve la fecha de serie del libro
   def fecha
   	 	@fecha_serie
   	 
   end
  #metodo que devuelve la serie del libro
   def set_serie(serie)
   	 	@serie=serie
   	 
   end
  #metodo que devuelve la editorial del libro
   def editorial
   	 	@editorial
   	 
   end
   #metodo que devuelve el numero de edicion.
   def edicion
   	 	@num_edicion
   	 
   end
   
#metodo que muestra la referencia formateada de la biblioteca 
   
   def formato
   puts "------------------------------------------------------------------------------------"
    @autor.each do |aut|
      print aut+","
    end
    puts "\n"+@titulo
    print @serie
    puts @editorial+" : "+edicion+" "+@fecha_serie
    
    #añadi self.isbn porque por algun motivo mostraba dos veces el contenido del array
    
    puts self.isbn
    puts "------------------------------------------------------------------------------------"
   end
   
   
end




